package com.annimon.militaryhero;

import com.annimon.jecp.se.Application;

/**
 * @author aNNiMON
 */
public class Main extends Application {

    public static void main(String[] args) {
        new Main();
    }

    public Main() {
        super(new MainApp(), 640, 480);
        setTitle("Military Hero");
    }
}