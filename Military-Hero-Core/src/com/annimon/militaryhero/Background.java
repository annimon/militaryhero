package com.annimon.militaryhero;

import com.annimon.jecp.Graphics;
import com.annimon.jecp.Image;

/**
 * Draws background by separate layers.
 * @author aNNiMON
 */
public class Background {
    
    private final Image background, obstacles;

    public Background() {
        background = ImageLoader.load(ImageLoader.BACKGROUND);
        obstacles = ImageLoader.load(ImageLoader.OBSTACLES);
    }
    
    public void draw(Graphics g) {
        g.drawImage(background, 0, 0);
    }
    
    public void drawObstacles(Graphics g) {
        g.drawImage(obstacles, 0, 200);
    }
}
