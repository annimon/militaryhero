package com.annimon.militaryhero;

import com.annimon.jecp.ApplicationListener;
import com.annimon.jecp.Graphics;
import com.annimon.jecp.Jecp;
import com.annimon.jecp.Keys;

/**
 * Main loop.
 * @author aNNiMON
 */
public class MainApp extends InputProcessor implements ApplicationListener {
    
    /** Coords for enemies */
    private static final int[] COORD = {
        6, 321, 49, 318, 100, 228, 145, 259, 230, 356,
        86, 375, 365, 325, 344, 232, 296, 296, 457, 355,
        480, 260, 530, 288, 500, 288, 590, 297, 617, 343,
        2, 467, 502, 7
    };
    
    private int width, height;
    private Background background;
    private SniperAim aim;
    private Enemy[] enemies;
    
    public void onStartApp(int width, int height) {
        this.width = width;
        this.height = height;
        background = new Background();
        aim = new SniperAim(width, height);
        
        enemies = new Enemy[COORD.length / 2];
        for (int i = 0; i < enemies.length; i++) {
            enemies[i] = new Enemy(COORD[i * 2], COORD[i * 2 + 1]);
        }
        GameState.init(enemies.length);
        
        Jecp.inputListener = this;
        Keys.numericdAsDpad = true;
        Keys.wasdAsDpad = true;
    }

    public void onPauseApp() {
    }

    public void onDestroyApp() {
    }

    public void onPaint(Graphics g) {
        background.draw(g);
        for (int i = 0; i < enemies.length; i++) {
            enemies[i].draw(g);
        }
        // We need to draw enemies BEFORE obstacles.
        background.drawObstacles(g);
        aim.draw(g);
        GameState.drawState(g, width, height);
    }

    public void onUpdate() {
        // Processing aim movings.
        if (isPressed(LEFT)) aim.move(-1, 0);
        else if (isPressed(RIGHT)) aim.move(1, 0);
        if (isPressed(UP)) aim.move(0, -1);
        else if (isPressed(DOWN)) aim.move(0, 1);
            
        try {
            Thread.sleep(10);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public void isFirePressed() {
        if (GameState.isEndGame()) return;

        // Check collides with aim and recalculate remaining enemies.
        final int x = aim.getAimX();
        final int y = aim.getAimY();
        int counter = 0;
        for (int i = 0; i < enemies.length; i++) {
            if (enemies[i].isVisible() && !enemies[i].isCollide(x, y)) {
                counter++;
            }
        }
        GameState.setEnemiesLeft(counter);
        GameState.decreaseBulletLeft();
    }
    
    public void onPointerPressed(int x, int y) {
        aim.set(x, y);
    }
    
    public void onPointerReleased(int x, int y) {
        isFirePressed();
    }

    public void onPointerDragged(int x, int y) {
        aim.set(x, y);
    }
}
