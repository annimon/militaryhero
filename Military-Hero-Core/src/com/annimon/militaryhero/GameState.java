package com.annimon.militaryhero;

import com.annimon.jecp.Graphics;

/**
 * Class for game states.
 * @author aNNiMON
 */
public class GameState {
    
    private static int bulletLeft, enemiesLeft;

    public static void init(int enemiesLeft) {
        GameState.bulletLeft = enemiesLeft + enemiesLeft / 2;
        GameState.enemiesLeft = enemiesLeft;
    }
    
    public static void decreaseBulletLeft() {
        bulletLeft--;
    }

    public static void setEnemiesLeft(int enemiesLeft) {
        GameState.enemiesLeft = enemiesLeft;
    }
    
    public static boolean isEndGame() {
        return (bulletLeft == 0) || (enemiesLeft == 0);
    }
    
    public static void drawState(Graphics g, int width, int height) {
        g.setColor(0xFFFFFFFF);
        g.drawString("Bullets: " + bulletLeft, 0, 0);
        g.drawString("Enemies: " + enemiesLeft, 0, g.getTextHeight() + 5);
        if (isEndGame()) {
            g.setColor(0xFF000000);
            g.fillRect(0, 0, width, height);
            final boolean winState = (enemiesLeft == 0);
            g.setColor(winState ? 0xFF00FF00 : 0xFFFF0000);
            final String message = winState ? "CONGRATULATIONS" : "GAME OVER";
            final int x = (width - g.getTextWidth(message)) / 2;
            final int y = (height - g.getTextHeight()) / 2;
            g.drawString(message, x, y);

        }
    }
}
