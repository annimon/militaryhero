package com.annimon.militaryhero;

import com.annimon.jecp.Graphics;
import com.annimon.jecp.Image;

/**
 * @author aNNiMON
 */
public class SniperAim {

    private final Image aim;
    private final int aimWidth, aimHeight;
    private final int width, height;
    private int x, y;

    public SniperAim(int width, int height) {
        this.width = width;
        this.height = height;
        aim = ImageLoader.load(ImageLoader.SNIPER_AIM);
        aimWidth = aim.getWidth();
        aimHeight = aim.getHeight();
        x = (width - aimWidth) / 2;
        y = (height - aimHeight) / 2;
    }
    
    /**
     * Move aim by relative coordinates.
     * @param dx horizontal offset.
     * @param dy vertical offset.
     */
    public void move(int dx, int dy) {
        x += dx;
        y += dy;
  
        // Check bounds horizontally.
        final int aw2 = aimWidth / 2;
        if (x < -aw2) x = -aw2;
        else if (x + aw2 > width) x = width - aw2;
        // Check bounds vertically.
        final int ah2 = aimHeight / 2;
        if (y < -ah2) y = -ah2;
        else if (y + ah2 > height) y = height - ah2;
    }
    
    /**
     * Set aim to absolute coordinates.
     * @param x new horizontal position.
     * @param y new vertical position.
     */
    public void set(int x, int y) {
        this.x = x - aimWidth / 2;
        this.y = y - aimHeight / 2;
    }
    
    public int getAimX() {
        return x + aimWidth / 2;
    }
    
    public int getAimY() {
        return y + aimHeight / 2;
    }
    
    public void draw(Graphics g) {
        g.drawImage(aim, x, y);
        // Fill screen around aim image.
        g.setColor(0xFF000000);
        final int bottom = y + aimHeight;
        g.fillRect(0, 0, width, y);
        g.fillRect(0, bottom, width, height - bottom);
        final int right = x + aim.getWidth();
        g.fillRect(0, y, x, aimHeight);
        g.fillRect(right, y, width - right, aimHeight);
    }
}
