package com.annimon.militaryhero;

import com.annimon.jecp.Image;
import java.io.IOException;

/**
 * Simply image loader.
 * @author aNNiMON
 */
public class ImageLoader {
    
    public static final String 
            SNIPER_AIM = "sniper_aim.png",
            BACKGROUND = "background.jpg",
            OBSTACLES = "obstacles.png",
            ENEMY = "enemy.png";
    
    /**
     * Load image without checking file existing.
     * @param name image name with extension.
     * @return Image object or null.
     */
    public static Image load(String name) {
        try {
            return Image.createImage("res/" + name);
        } catch (IOException ex) {
            return null;
        }
    }
}
