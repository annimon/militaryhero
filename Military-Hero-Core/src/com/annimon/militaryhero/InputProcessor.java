package com.annimon.militaryhero;

import com.annimon.jecp.InputListener;
import com.annimon.jecp.Keys;

/**
 * Processing keys and pointer actions.
 * @author aNNiMON
 */
public abstract class InputProcessor implements InputListener {
    
    public static final int LEFT = 0, UP = 1, RIGHT = 2, DOWN = 3, FIRE = 4;
    
    /** Stores dpad-keys pressed state */
    private final boolean[] dpadState;
    
    public InputProcessor() {
        dpadState = new boolean[4];
    }
    
    public boolean isPressed(int dpad) {
        return dpadState[dpad];
    }
    
    public abstract void isFirePressed();

    public void onKeyPressed(int key) {
        final int dpad = Keys.convertToDpad(key);
        if (dpad == Keys.DPAD_FIRE) {
            isFirePressed();
        } else setDpadState(dpad, true);
    }

    public void onKeyReleased(int key) {
        final int dpad = Keys.convertToDpad(key);
        setDpadState(dpad, false);
    }
    
    private void setDpadState(int dpad, boolean state) {
        switch (dpad) {
            case Keys.DPAD_LEFT:
                dpadState[LEFT] = state;
                break;
            case Keys.DPAD_UP:
                dpadState[UP] = state;
                break;
            case Keys.DPAD_RIGHT:
                dpadState[RIGHT] = state;
                break;
            case Keys.DPAD_DOWN:
                dpadState[DOWN] = state;
                break;
        }
    }
}
