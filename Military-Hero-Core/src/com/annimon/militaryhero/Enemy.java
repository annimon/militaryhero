package com.annimon.militaryhero;

import com.annimon.jecp.Graphics;
import com.annimon.jecp.Image;

/**
 * @author aNNiMON
 */
public class Enemy {
    
    private static final Image enemy = ImageLoader.load(ImageLoader.ENEMY);
    private static final int enemyWidth = enemy.getWidth();
    private static final int enemyHeight = enemy.getHeight();
    
    private final int x, y;
    private boolean visible;

    public Enemy(int x, int y) {
        this.x = x;
        this.y = y;
        visible = true;
    }
    
    public boolean isVisible() {
        return visible;
    }
    
    public boolean isCollide(int sx, int sy) {
        if (inRange(sx, x, x + enemyWidth) && inRange(sy, y, y + enemyHeight)) {
            // If bullet collide enemy - hide them.
            visible = false;
            return true;
        }
        return false;
    }
    
    public void draw(Graphics g) {
        if (visible) {
            g.drawImage(enemy, x, y);
        }
    }
    
    private boolean inRange(int value, int min, int max) {
        return (value >= min) && (value <= max);
    }
}
