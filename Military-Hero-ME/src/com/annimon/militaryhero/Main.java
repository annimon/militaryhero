package com.annimon.militaryhero;

import com.annimon.jecp.me.Application;

/**
 * @author aNNiMON
 */
public class Main extends Application {

    public Main() {
        super(new MainApp(), true);
    }
}
